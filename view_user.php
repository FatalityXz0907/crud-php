<?php 
  include "header.php";
  include "database.php";
    if(isset($_GET['delete'])) {
      $delete_id = $_GET['delete'];
      $delete = "DELETE FROM user WHERE user_id = '$delete_id'";
      $run_delete = mysqli_query($conn, $delete);
      if($run_delete === true) {
        echo "Record has been deleted";
      } else {
        echo "Failed, Try again";
      }
}?>
<div class="card">
  <h2 class="card-title text-center mt-3">User Table Page</h2>
  <div class="card-body">
      <table class="table table-striped table-hover w-75 m-auto mt-3 border">
      <thead>
        <tr>
            <th class="text-left">ID</th>
            <th class="text-left">Name</th>
            <th class="text-left">Email</th>
            <th class="text-left">Password</th>
            <th class="text-left">Image</th>
            <th class="text-left">Detail</th>
            <th class="text-left">Option</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          include "database.php";
        //show data
          $select = "SELECT * FROM user";
          $run = mysqli_query($conn, $select);
          while ($row_user = mysqli_fetch_array($run)) {
            $user_id = $row_user['user_id'];
            $user_name = $row_user['user_name'];
            $user_email = $row_user['user_email'];
            $user_password = $row_user['user_password'];
            $user_image = $row_user['user_image'];
            $user_detail = $row_user['user_details'];            
        ?>
        <tr>
          <td class="text-left"><?= $user_id;?></td>
          <td class="text-left"><?= $user_name;?></td>
          <td class="text-left"><?= $user_email;?></td>
          <td class="text-left"><?= $user_password;?></td>
          <td class="text-left"><img src="upload/<?= $user_image;?>" style="width: 50px; height: 50px ;"></td>
          <td class="text-left"><?= $user_detail;?></td>
          <td class="text-left">
            
            <a href="edit_user.php?edit=<?= $user_id;?>">
              <svg style="width: 1.5rem; text-decoration: none; color: #334155;" xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z" />
              </svg>
            </a>  
            <a href="view_user.php?delete=<?= $user_id;?>">
              <svg style="width: 1.5rem; text-decoration: none; color: #334155;" xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" />
              </svg>
            </a>               
            
          </td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
</div>
<?php include "footer.php"?>