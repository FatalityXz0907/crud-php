<?php
    include "header.php";
    session_start();
    include "database.php";
    if(!isset($_SESSION['user_name'])) {
      header('Location: login.php');
    }
?>

<nav class="navbar fixed-top bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Home</a>
    <a href="view_user.php" class="btn btn-primary">Check Users</a>
    <a class="btn btn-danger" href="logout.php">Log out</a>
  </div>
</nav>



<?php
    include "footer.php";
?>