<?php
    include "header.php";
    session_start();
?>
<div class="container mt-5">
    <div class="card">
        <h2 class="card-title text-center">
            Login Page
        </h2>
        <div class="card-body">
            <form style="width: 50%; margin: auto;" action="#" method="post">
                <div class="mb-3">
                    <label for="username" class="form-label">Username : </label>
                    <input type="text" class="form-control" id="username" name="name">
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password : </label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="mt-4 mb-4 w-50 m-auto" style="display: flex; justify-content: space-between;">
                    <button class="btn btn-primary" name="login">Login</button>
                    <button class="btn btn-primary" name="create">Create account</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
    include "database.php";
    if(isset($_POST['create'])) {
        header('Location: create_user.php');
    }
    if(isset($_POST['login'])) {
        $user_name = $_POST['name'];
        $user_password = $_POST['password'];
        $select = "SELECT * FROM user WHERE user_name = '$user_name'";
        $run_select = mysqli_query($conn, $select);
        if($run_select) {
            $row_user = mysqli_fetch_array($run_select);
            $db_username = $row_user['user_name'];
            $db_user_password = $row_user['user_password'];
            if($user_name == $db_username && $user_password == $db_user_password) {
                header("Location: home.php");
                $_SESSION['user_name'] = $db_username;
            } else {
                echo "Email or Password is wrong!";
                }   
            }  
    }

?>


<?php
    include "footer.php";
?>