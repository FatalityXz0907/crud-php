<?php   
    include "header.php";
    include "database.php";
?>
    <?php
        if(isset($_GET['edit'])) {
            $edit_id = $_GET['edit'];
            $select = "SELECT * FROM user WHERE user_id = '$edit_id'";
            $run = mysqli_query($conn, $select);
            $row_user = mysqli_fetch_array($run);
            $user_name = $row_user['user_name'];
            $user_email = $row_user['user_email'];
            $user_password = $row_user['user_password'];
            $user_image = $row_user['user_image'];
            $user_detail = $row_user['user_details'];
        }
    ?>


    <div class="mb-3 card" >
        <div class="card-body">
            <h2 class="card-title text-center">
                Edit User Page
            </h2>
            <form style="width: 50%; margin: auto; margin-top: 30px;" action="" method="post" enctype="multipart/form-data">
        
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Username : </label>
                    <input value="<?= $user_name;?>" type="text" class="form-control" id="exampleInputEmail1" name="name">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Email : </label>
                    <input value="<?= $user_email;?>" type="email" class="form-control" id="exampleInputPassword1" name="email">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Password : </label>
                    <input value="<?= $user_password;?>" type="password" class="form-control" id="exampleInputPassword1" name="password">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Image : </label>
                    <input value="<?= $user_image;?>" type="file" class="form-control" id="exampleInputPassword1" name="image">
                </div>
                <label for="exampleInputPassword1" class="form-label">Detail : </label>
                <textarea name="detail" class="form-control"><?= $user_detail;?></textarea>
                <br>
                <button type="submit" name="update" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
    
    <?php
        if(isset($_POST['update'])) {
            $ename = $_POST['name'];
            $eemail = $_POST['email'];
            $epassword = $_POST['password'];
            $eimage = $_FILES['image']['name'];
            $etmp_name = $_FILES['image']['tmp_name'];
            $edetail = $_POST['detail'];
            if(empty($eimage)) {
                $eimage = $user_image;
            }
            $update = "UPDATE user SET user_name ='$ename', user_email='$eemail', user_password= '$epassword',
                       user_image = '$eimage', user_details = '$edetail' WHERE user_id = '$edit_id'";
            $run_update = mysqli_query($conn, $update);
            if($run_update === true) {
                echo "Data has been updated"; 
                move_uploaded_file($etmp_name, "upload/$eimage");
            } else {
                echo "Error";
            }
            header('Location: view_user.php');
        }
    ?>
    
    
<?php include "footer.php"?>